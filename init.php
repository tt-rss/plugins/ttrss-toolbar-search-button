<?php
class Search_Button extends Plugin {

	function about() {
		return array(1.0,
			"Adds a toolbar button to open search dialog",
			"fox");
	}

	function init($host) {
		$host->add_hook($host::HOOK_MAIN_TOOLBAR_BUTTON, $this);
	}

	function hook_main_toolbar_button() {
		?>

		<button dojoType="dijit.form.Button" onclick="Feeds.search()" style="order : 25">
			<i class="material-icons" title="<?php echo __('Search...') ?>">search</i>
		</button>

		<?php
	}

	function api_version() {
		return 2;
	}
}
